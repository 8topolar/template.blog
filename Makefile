clean:
	rm -rf public

build:
	./scripts/build

deploy:
	ipfs.deploy public

serve:
	python -m http.server --directory public/
